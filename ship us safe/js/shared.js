var rootUrl = "file:///Users/Sachin/Projects/Guliya%20CW/ship%20us%20safe"


class Ship {
    range;
    maxSpeed;
    name;
    description;
    status;
    cost;
    constructor(range, maxSpeed, name, description, status,cost) {
        this.range = range;
        this.maxSpeed = maxSpeed;
        this.name = name;
        this.description = description;
        this.status = status;
        this.cost = cost
    }
}

class Port {
    name;
    country;
    type;
    size;
    longitude;
    latitude;
    constructor(name, country, type, size, longitude, latitude) {
        this.name = name;
        this.country = country;
        this.type = type;
        this.size = size;
        this.longitude = longitude;
        this.latitude = latitude;
    }
}

class Route {
    name;
    ship;
    sourcePort;
    destinationPort;
    distance;
    time;
    cost;
    startDate;
    wayPointList;

    constructor(name, ship, sourcePort, destinationPort, distance, time, cost, startDate, wayPointList) {
        this.name = name;
        this.ship = ship;
        this.sourcePort = sourcePort;
        this.destinationPort = destinationPort;
        this.distance = distance;
        this.time = time;
        this.cost = cost;
        this.startDate = startDate;
        this.wayPointList = wayPointList;
    }
}

class ShipList{
    ships;
    constructor(ships=[]){ // assigning an empty array to the parameter as a default value. When creating this instance if the parameter is not passed it will create a class with an empty array
        this.ships = ships;
    }
}

class RouteList{
    routes;
    constructor(routes=[]){ // assigning an empty array to the parameter as a default value. When creating this instance if the parameter is not passed it will create a class with an empty array
        this.routes = routes;
    }
}

class PortList{
    ports;
    constructor(ports=[]){ // assigning an empty array to the parameter as a default value. When creating this instance if the parameter is not passed it will create a class with an empty array
        this.ports = ports;
    }
}


window.onload = () => {
    const routeList = JSON.parse(window.localStorage.getItem("route-list"));
    let html = null
    if (routeList) {
        html = routeList.map((route) => {
            return (
                `<div class="mdl-cell mdl-cell--4-col-desktop mdl-cell--4-col-tablet mdl-cell--0-col-phone">
                    <div class="demo-card-wide mdl-card mdl-shadow--2dp">
                        <div class="mdl-card__title">
                            <div>
                                <div class="mdl-card__title-text">${route.name}</div>
                            </div>
                        </div>
                        <div class="mdl-card__supporting-text">
                            <div>
                                <span class="mdl-cell mdl-cell--5-col">Country</span>
                                <span class="mdl-cell mdl-cell--5-col text-right">${route.country} knots</span>
                            </div>
                            <div>
                                <span class="mdl-cell mdl-cell--5-col" style="width: 100%;">Type</span>
                                <span class="mdl-cell mdl-cell--5-col text-right">${route.type} km</span>
                            </div>
                            <div>
                                <span class="mdl-cell mdl-cell--5-col" style="width: 100%;">Size</span>
                                <span class="mdl-cell mdl-cell--5-col text-right">Rs.${route.size}</span>
                            </div>
                            <div>
                                <span class="mdl-cell mdl-cell--5-col" style="width: 100%;">Location</span>
                                <span class="mdl-cell mdl-cell--5-col text-right">${route.longitude}</span>
                            </div>
                        </div>
                    </div>
                </div>`
            )
        }).join('')
    }else{
        html = `<div class="mdl-cell mdl-cell--8-col-desktop mdl-cell--2-offset-desktop mdl-cell--8-col-tablet mdl-cell--4-col-phone text-center heading">
                    <div class="demo-card-wide mdl-card mdl-shadow--2dp text-center">
                        <div class="mdl-card__title">
                            <div>
                                <div class="mdl-card__title-text text-center">No Routes Created</div>
                                <div class="mdl-card__title-text description-text text-center">Create a route to start with</div>
                            </div>
                        </div>
                    </div>
                </div>`
    }
    document.getElementById("route-list").innerHTML = html;
}
function onCreateRoute() {
    window.location = `${rootUrl}/addRoute.html`
}
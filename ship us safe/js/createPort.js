
function onSavePort(){
    const name = document.getElementById("name").value;
    const country = document.getElementById("country").value;
    const type = document.getElementById("type").value;
    const size = document.getElementById("size").value;
    const location = document.getElementById("location").value;
    const port = new Port(name,country,type,size,location,location);
    const savedPortList = window.localStorage.getItem("port-list");
    const portList = new PortList(savedPortList ? JSON.parse(savedPortList) : []);
    portList.ports.push(port);
    window.localStorage.setItem("port-list", JSON.stringify(portList.ports));
    navigateBack();
}

function navigateBack(){
    window.location = `${rootUrl}/viewShips.html`
}

window.onload = () => {
    const shipList = JSON.parse(window.localStorage.getItem("ship-list"));
    let html = null
    if (shipList) {
        html = shipList.map((ship) => {
            return (
                `<div class="mdl-cell mdl-cell--4-col-desktop mdl-cell--4-col-tablet mdl-cell--0-col-phone">
                    <div class="demo-card-wide mdl-card mdl-shadow--2dp">
                        <div class="mdl-card__title">
                            <div>
                                <div class="mdl-card__title-text">${ship.name}</div>
                                <div class="mdl-card__title-text description-text text-center">${ship.description}</div>
                            </div>
                        </div>
                        <div class="mdl-card__supporting-text">
                            <div>
                                <span class="mdl-cell mdl-cell--5-col">Max Speed</span>
                                <span class="mdl-cell mdl-cell--5-col text-right">${ship.maxSpeed} knots</span>
                            </div>
                            <div>
                                <span class="mdl-cell mdl-cell--5-col" style="width: 100%;">Range</span>
                                <span class="mdl-cell mdl-cell--5-col text-right">${ship.range} km</span>
                            </div>
                            <div>
                                <span class="mdl-cell mdl-cell--5-col" style="width: 100%;">Cost</span>
                                <span class="mdl-cell mdl-cell--5-col text-right">Rs.${ship.cost}</span>
                            </div>
                            <div>
                                <span class="mdl-cell mdl-cell--5-col" style="width: 100%;">Status</span>
                                <span class="mdl-cell mdl-cell--5-col text-right">${ship.status}</span>
                            </div>
                        </div>
                    </div>
                </div>`
            )
        }).join('')
    }else{
        html = `<div class="mdl-cell mdl-cell--8-col-desktop mdl-cell--2-offset-desktop mdl-cell--8-col-tablet mdl-cell--4-col-phone text-center heading">
                    <div class="demo-card-wide mdl-card mdl-shadow--2dp text-center">
                        <div class="mdl-card__title">
                            <div>
                                <div class="mdl-card__title-text text-center">No Ships Created</div>
                                <div class="mdl-card__title-text description-text text-center">Create a ship to start with</div>
                            </div>
                        </div>
                    </div>
                </div>`
    }
    document.getElementById("ship-list").innerHTML = html;
}
function onCreateShip() {
    window.location = `${rootUrl}/createShip.html`
}
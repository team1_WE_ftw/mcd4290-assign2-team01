
function onSaveShip(){
    const name = document.getElementById("name").value;
    const speed = document.getElementById("max-speed").value;
    const range = document.getElementById("range").value;
    const cost = document.getElementById("cost").value;
    const description = document.getElementById("description").value;
    const status = "active"
    const ship = new Ship(range,speed,name,description,status,cost);
    const savedShipList = window.localStorage.getItem("ship-list");
    const shipList = new ShipList(savedShipList ? JSON.parse(savedShipList) : []);
    shipList.ships.push(ship);
    window.localStorage.setItem("ship-list", JSON.stringify(shipList.ships));
    navigateBack();
}

function navigateBack(){
    window.location = `${rootUrl}/viewShips.html`
}